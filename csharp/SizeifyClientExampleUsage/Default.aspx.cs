﻿using System;
using SJC;

namespace SizeifyClientTest {
   public partial class Default : System.Web.UI.Page {

      public string title = "SJC Sizeify Test Page for C# Client";

      protected async void Page_Load(object sender, EventArgs e) {
         SizeifyClient sc = new SizeifyClient();
         try {
            image.Src = await sc.Sizeify(
               "http://sizeifyb.sjc.io/",
               "http://placekitten.com/1000/",
               "w50"
            );
         }
         catch (Exception ex) {
            errorMsg.InnerText = ex.Message;
         }
      }
   }
}