﻿<%@ Page Async="true" Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SizeifyClientTest.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title><%=title%></title>
</head>
<body>
   <form id="form1" runat="server">
      <div>
         <h1><%=title%></h1>
         <img runat="server" id="image" src="loading.gif" />
         <p runat="server" id="errorMsg"></p>
      </div>
   </form>
</body>
</html>