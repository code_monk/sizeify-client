# C&#35; Sizefy Client for .NET 4.51 

This Visual Studio 2015 solution wraps three projects, SizeifyClientLibrary and SizeifyClientExampleUsage and SizeifyClientTests. 

* SizeifyClientLibrary: contains source code to build a portable client library that can be referenced in other .NET projects
* SizeifyClientExampleUsage: a simple ASP.NET Webforms project that demonstrates how to reference and use the SizeifyClientLibrary.
* SizeifyClientTests: Unit tests

For convenience a 64 bit compiled DLL is available in the dist folder as [SizeifyClientLibrary.dll](https://github.com/stjosephcontent/sizeify-client/blob/master/csharp/dist/SizeifyClientLibrary.dll).