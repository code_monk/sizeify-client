﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SJC {

   public class SizeifyClient {
       
      /// <summary>
      /// An asynchronous GET to an SJC Sizeify HTTP API
      /// </summary>
      /// <param name="endpoint">
      /// The API endpoint. For production this is typically http://sizeifyb.sjc.io/
      /// </param>
      /// <param name="imageURL">
      /// Public URI to a valid web ready image
      /// </param>
      /// <param name="resizeCode">
      /// One of the valid Sizeify codes
      /// </param>
      /// <returns>
      /// A base 64 encoded string formatted as an image data URI.
      /// </returns>
      public async Task<string> Sizeify(string endpoint, string imageURL, string resizeCode) {
         var httpClient = new HttpClient();

         try {
            var response = await httpClient.GetAsync(formatURL(endpoint, imageURL, resizeCode));
            response.EnsureSuccessStatusCode();

            byte[] content = await response.Content.ReadAsByteArrayAsync();
            string mediaType = response.Content.Headers.ContentType.MediaType;
            string base64Data = Convert.ToBase64String(content);
            string dataURI = "data:" + mediaType + "; base64," + base64Data;

            return await Task.Run(() => dataURI);
         }
         catch (HttpRequestException ex) {
            throw ex;
         }
      }

      /// <summary>
      /// Takes the supplied parameters and returns a URL specifically formated for the Sizeify HTTP API
      /// </summary>
      /// <param name="endpoint">
      /// The API endpoint. For production this is typically http://sizeifyb.sjc.io/
      /// </param>
      /// <param name="imageURL">
      /// Public URI to a valid web ready image
      /// </param>
      /// <param name="resizeCode">
      /// One of the valid Sizeify codes
      /// </param>
      /// <returns>
      /// String representation of the fully qualified Sizeify HTTP API request URL
      /// </returns>
      private string formatURL(string endpoint, string imageURL, string resizeCode) {
         // endpoint:   http://sizeifyb.sjc.io/
         // imageURL:   http://placekitten.com/1000
         // resizeCode: w50
         // sizeifyURL: http://sizeifyb.sjc.io/http/com.placekitten/w50/1000

         // Trim any trailing slashes
         endpoint = new Regex("/[^/]*$").Replace(endpoint, "");
         imageURL = new Regex("/[^/]*$").Replace(imageURL, "");

         List<string> segments = new List<string>();
         Uri uri = new Uri(imageURL);

         string[] imageHostParts = uri.Host.Split('.');
         Array.Reverse(imageHostParts);
         string imageHost = String.Join(".", imageHostParts);

         segments.Add(endpoint);
         segments.Add((uri.IsDefaultPort) ? uri.Scheme : (uri.Scheme + ":" + uri.Port));
         segments.Add(imageHost);
         segments.Add(resizeCode);
         segments.Add(uri.LocalPath.Substring(1, uri.LocalPath.Length - 1));

         return String.Join("/", segments);
      }
   }
}