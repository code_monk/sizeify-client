#!/usr/bin/python
# -*- coding: utf8 -*-
import unittest
from python.sizeify import sizeify

'''
How to run:

    $ cd sizeify-client
    $ PYTHONPATH=. python test/test.py
'''


class TestUrl(unittest.TestCase):

    def test_default_endpoint(self):
        self.assertEqual(
            sizeify.url('http://hdwallpapersfit.com/wp-content/uploads/2015/01/cool-chuck-norris-wallpapers-american-actor.jpg', 'w450'),
            'https://sizeifyb.sjc.io/http/com.hdwallpapersfit/w450/wp-content%2Fuploads%2F2015%2F01%2Fcool-chuck-norris-wallpapers-american-actor.jpg'
        )
        self.assertEqual(
            sizeify.url('http://iet.jrc.ec.europa.eu/energyefficiency/sites/energyefficiency/files/images/organisation/part_logo_superu.gif', 'c500'),
            'https://sizeifyb.sjc.io/http/eu.europa.ec.jrc.iet/c500/energyefficiency%2Fsites%2Fenergyefficiency%2Ffiles%2Fimages%2Forganisation%2Fpart_logo_superu.gif'
        )
        self.assertEqual(
            sizeify.url('http://sizeifyb-lowres.sjc.io', 'xxxx'),
            'https://sizeifyb.sjc.io/error?code=400'
        )

    def test_change_endpoint(self):
        old_endpoint = sizeify.endpoint
        sizeify.endpoint = 'http://sizeifyb-hq.sjc.io'
        self.assertEqual(
            sizeify.url('http://sizeifyb-highres.sjc.io/http/net.nocookie.wikia.img1/w777/__cb20120811205503%2Fadventuretimewithfinnandjake%2Fimages%2Ff%2Ffc%2FS4_E18_Finn_weird.PNG', 'w300'),
            'https://sizeifyb-hq.sjc.io/http/io.sjc.sizeifyb-highres/w300/http%2Fnet.nocookie.wikia.img1%2Fw777%2F__cb20120811205503%252Fadventuretimewithfinnandjake%252Fimages%252Ff%252Ffc%252FS4_E18_Finn_weird.PNG'
        )
        self.assertEqual(
            sizeify.url('http://hdwallpapersfit.com/wp-content/uploads/2015/01/cool-chuck-norris-wallpapers-american-actor.jpg', 'b320'),
            'https://sizeifyb-hq.sjc.io/http/com.hdwallpapersfit/b320/wp-content%2Fuploads%2F2015%2F01%2Fcool-chuck-norris-wallpapers-american-actor.jpg'
        )
        self.assertEqual(
            sizeify.url(u'https://s3.amazonaws.com/brain-assets-non-prod/brain$ test , ¢£ª£.jpg', 'b320'),
            'https://sizeifyb-hq.sjc.io/https/com.amazonaws.s3/b320/brain-assets-non-prod%2Fbrain%24%20test%20%2C%20%C2%A2%C2%A3%C2%AA%C2%A3.jpg'
        )
        sizeify.endpoint = old_endpoint


if __name__ == '__main__':
    unittest.main()
