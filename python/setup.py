from distutils.core import setup

setup(
    name="sizeify",
    packages=["sizeify"],
    version="1.2.2",
    description="official client for sizeify, the image resizing and hosting service",
    author="Sean Macdonald",
    author_email="sean.macdonald@stjoseph.com",
    url="https://sizeifyb.sjc.io",
    download_url="https://github.com/stjosephcontent/sizeify-client/archive/v1.2.1.zip",
    keywords=["image", "resize", "cdn"],
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Development Status :: 4 - Beta",
        "Environment :: Other Environment",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Multimedia :: Graphics :: Graphics Conversion"
    ]
);
