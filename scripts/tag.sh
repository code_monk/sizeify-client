#!/bin/bash
set -e

if [ $# -ne 2 ]; then
    echo "Usage: tag.sh version message"
    exit 1
fi

git checkout master
git pull

# check if tag already exists on machine, if so, delete it
if git tag -l "$1" | grep -Fx $1; then
    git tag --delete "$1"
fi

# check if tag already exists on remote, if so, delete it
if git ls-remote --exit-code --tags origin | cut -f 2 | grep -Fx "refs/tags/$1"; then
    git push origin :$1
fi

git tag -a "$1" -m "$2"
git push --tags
